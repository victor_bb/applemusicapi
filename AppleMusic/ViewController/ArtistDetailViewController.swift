//
//  ArtistDetailViewController.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 28.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

let albumReuseIdentifier = "albumView"
let songReuseIdentifier = "songReuseIdentifier"

class ArtistDetailViewController: UIViewController {

    let albumViewModel = AlbumLookupViewModel()
    let songViewModel = SongLookupViewModel()
    var currentArtist: Artist?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let artist = currentArtist {
            collectionView.delegate = self
            collectionView.dataSource = self
            
            tableView.delegate = self
            tableView.dataSource = self
            tableView.prefetchDataSource = self
            
            navigationItem.title = artist.artistName

            albumViewModel.delegate = self
            albumViewModel.lookup(artist: artist, type: .album)
            
            songViewModel.delegate = self
            songViewModel.lookup(artist: artist, type: .song)
        }
        
        // TODO images to cache!
    }

}

extension ArtistDetailViewController: UICollectionViewDelegate {
    
}

extension ArtistDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albumViewModel.currentCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: albumReuseIdentifier, for: indexPath) as! AlbumCollectionViewCell
        let album = albumViewModel.album(at: indexPath.row)
        
        cell.label.text = album.collectionName
        cell.imagePath = album.artworkUrl100
        
        return cell
    }
    
    
}

extension ArtistDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songViewModel.totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: songReuseIdentifier, for: indexPath)
        
        if isLoadingCell(for: indexPath) {
            cell.textLabel?.text = "loading \(indexPath.row)"
        } else {
            let song = songViewModel.song(at: indexPath.row)
            cell.textLabel?.text = song.collectionName
            cell.detailTextLabel?.text = song.trackName
        }
        
        
        return cell
    }
    
    
}

extension ArtistDetailViewController: AlbumLookupViewModelDelegate {
    func onAlbumFetchCompleted(with newIndexPathsToReload: [IndexPath]?) {
        self.collectionView.reloadData()
    }
}

extension ArtistDetailViewController: SongLookupViewModelDelegate {
    func onSongsFetchCompleted(with newIndexPathsToReload: [IndexPath]?) {
        self.tableView.reloadData()
    }
}

extension ArtistDetailViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            print("prefetch")
            
            songViewModel.lookup(artist: currentArtist!, type: .song)
        }
    }
}

private extension ArtistDetailViewController {
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        //print("row \(indexPath.row) > count\(songViewModel.currentCount)")
        
        return indexPath.row >= songViewModel.currentCount
    }

    func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
        let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows ?? []
        let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
        
        return Array(indexPathsIntersection)
    }
}
