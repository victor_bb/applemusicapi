//
//  MusicSearchViewController.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 18.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

private let textReuseIdentifier = "TextCell"
private let detailSegue = "DetailSegue"

class MusicSearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = SearchViewModel()
    
    var previousArtistNameEntered: String = ""
    var currentSearchingArtistName = ""
    var selectedArtist: Artist?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        selectedArtist = nil
        
        searchBar.delegate = self
        viewModel.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.prefetchDataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ArtistDetailViewController, segue.identifier == detailSegue {
            viewController.currentArtist = selectedArtist
        }
    }
    
    @objc func reloadSearchResults(request: String) {
        print("request: \(request)")
        
        currentSearchingArtistName = request
        
        if request.count == 0 {
            return
        }
        
        viewModel.findBy(artist: request)
    }
    

}

extension MusicSearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let artistName = searchBar.text ?? ""
        // to limit network activity, reload half a second after last key press.
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(reloadSearchResults), object: previousArtistNameEntered)
        self.perform(#selector(reloadSearchResults), with: artistName, afterDelay: 2)
        
        previousArtistNameEntered = artistName
    }
}

extension MusicSearchViewController: SearchViewModelDelegate {
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?) {
        guard let newIndexPathsToReload = newIndexPathsToReload else {
            //indicatorView.stopAnimating()
            //tableView.isHidden = false
            tableView.reloadData()

            return
        }
        
        let indexPathsToReload = visibleIndexPathsToReload(intersecting: newIndexPathsToReload)
        tableView.reloadRows(at: indexPathsToReload, with: .automatic)
    }
}

extension MusicSearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: textReuseIdentifier, for: indexPath)
        
        if isLoadingCell(for: indexPath) {
            cell.textLabel?.text = "loading \(indexPath.row)"
        } else {
            if let artist = viewModel.artist(at: indexPath.row) {
                cell.textLabel?.text = artist.artistName
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedArtist = viewModel.artist(at: indexPath.row)
        performSegue(withIdentifier: detailSegue, sender: nil)
//        let currentCell = tableView.cellForRow(at: indexPath) as! SettingsStandartTableViewCell
//
//        if currentCell.accessoryType == .checkmark {
//            selectedMapSection = nil
//            selectedCellIndex = nil
//
//            currentCell.accessoryType = .none
//            SettingsStore.shared.setMapSection(rawValue: 0)
//        } else {
//
//            if let lastSelectedIndex = selectedCellIndex {
//                let selectedIndexPath = IndexPath(item: lastSelectedIndex, section: 0)
//                let currentCellX = tableView.cellForRow(at: selectedIndexPath) as! SettingsStandartTableViewCell
//                currentCellX.accessoryType = .none
//            }
//
//            selectedCellIndex = indexPath.item
//            selectedMapSection = currentCell.section?.rawValue
//            currentCell.accessoryType = .checkmark
//            SettingsStore.shared.setMapSection(rawValue: selectedMapSection!)
//        }
    }
}

extension MusicSearchViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        print("prefetch")
        
        if indexPaths.contains(where: isLoadingCell) {
            viewModel.findBy(artist: currentSearchingArtistName)
        }
    }
}

private extension MusicSearchViewController {
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        print("row \(indexPath.row) > count\(viewModel.currentCount)")
        
        return indexPath.row >= viewModel.currentCount
    }

    func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
        let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows ?? []
        let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
        
        return Array(indexPathsIntersection)
    }
}
