//
//  UrlLookupBuilder.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 28.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import Foundation

class UrlLookupBuilder {
    var baseUrl: String = ""
    var limit: Int = 20
    var page: Int = 1
    var offset: Int = 0
    var urlArgs = [
        "id": "",
        "entity": "",
        "limit": "",
        "offset": ""
    ]
    
    func set(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func set(id: String) {
        urlArgs["id"] = id
    }
    
    func set(entity: String) {
        urlArgs["entity"] = entity
    }
    
    func set(limit: Int) {
        self.limit = limit
    }
    
    func set(page: Int) {
        self.page = page
    }
    
    func build() -> String {
        if page > 1 {
            offset = limit * (page - 1)
        }
        
        var argsString = ""
        
        for (key, value) in urlArgs {
            if value.count > 0 {
                argsString += "&\(key)=\(value)"
            }
        }
        
        argsString.remove(at: argsString.startIndex)

        argsString += "&limit=\(limit)&offset=\(offset)"
        
        print("argsString \(argsString)")
        
        return self.baseUrl + "?" + argsString
    }
}
