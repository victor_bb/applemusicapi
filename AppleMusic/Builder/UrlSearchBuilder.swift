//
//  UrlBuilder.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 20.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

class UrlSearchBuilder {
    
    var baseUrl: String = ""
    var page = 1
    var limit = 50
    var offset: Int = 0
    var urlArgs = [
        "term": "",
        "entity": "",
        "limit": "",
        "offset": ""
    ]
    
    func set(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func set(term: String) {
        urlArgs["term"] = term
    }
    
    func set(entity: String) {
        urlArgs["entity"] = entity
    }
    
    func set(limit: Int) {
        self.limit = limit
    }
    
    func set(page: Int) {
        self.page = page
    }
    
    func build() -> String {
        
        if page > 1 {
            offset = limit * (page - 1)
        }
        
        var argsString = ""
        
        for (key, value) in urlArgs {
            if value.count > 0 {
                argsString += "&\(key)=\(value)"
            }
        }
        
        argsString.remove(at: argsString.startIndex)
        
        argsString += "&limit=\(limit)&offset=\(offset)"
        
        print("argsString \(argsString)")
        //https://itunes.apple.com/search?term=lana&entity=musicArtist&limit=20&offset=800
        return self.baseUrl + "?" + argsString
    }
}
