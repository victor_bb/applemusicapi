//
//  Album.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 28.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import Foundation

class Album {
    let artistId: Int
    let collectionId: Int
    let amgArtistId: Int
    let artistName: String
    let collectionName: String
    let collectionCensoredName: String
    let artistViewUrl: String
    let collectionViewUrl: String
    let artworkUrl60: String
    let artworkUrl100: String
    let collectionPrice: Double
    let collectionExplicitness: String
    let trackCount: Int
    let copyright: String
    let country: String
    let currency: String
    let releaseDate: String
    let primaryGenreName: String
    
    init?(data: NSDictionary) {
        guard let wrapperType = data["wrapperType"] as? String,
            wrapperType == "collection",
            let collectionType = data["collectionType"] as? String,
            collectionType == "Album",
            let artistId = data["artistId"] as? Int,
            let collectionId = data["collectionId"] as? Int,
            let amgArtistId = data["amgArtistId"] as? Int,
            let artistName = data["artistName"] as? String,
            let collectionName = data["collectionName"] as? String,
            let collectionCensoredName = data["collectionCensoredName"] as? String,
            let artistViewUrl = data["artistViewUrl"] as? String,
            let collectionViewUrl = data["collectionViewUrl"] as? String,
            let artworkUrl60 = data["artworkUrl60"] as? String,
            let artworkUrl100 = data["artworkUrl100"] as? String,
            let collectionPrice = data["collectionPrice"] as? Double,
            let collectionExplicitness = data["collectionExplicitness"] as? String,
            let trackCount = data["trackCount"] as? Int,
            let copyright = data["copyright"] as? String,
            let country = data["country"] as? String,
            let currency = data["currency"] as? String,
            let releaseDate = data["releaseDate"] as? String,
            let primaryGenreName = data["primaryGenreName"] as? String else {
                return nil
        }
        
        self.artistId = artistId
        self.collectionId = collectionId
        self.amgArtistId = amgArtistId
        self.artistName = artistName
        self.collectionName = collectionName
        self.collectionCensoredName = collectionCensoredName
        self.artistViewUrl = artistViewUrl
        self.collectionViewUrl = collectionViewUrl
        self.artworkUrl60 = artworkUrl60
        self.artworkUrl100 = artworkUrl100
        self.collectionPrice = collectionPrice
        self.collectionExplicitness = collectionExplicitness
        self.trackCount = trackCount
        self.copyright = copyright
        self.country = country
        self.currency = currency
        self.releaseDate = releaseDate
        self.primaryGenreName = primaryGenreName
    }
}
