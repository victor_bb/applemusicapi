//
//  Song.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 30.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import Foundation

class Song {
    let collectionName: String
    let trackName: String
    let collectionCensoredName: String
    let trackCensoredName: String
    
    init?(data: NSDictionary) {
        guard let wrapperType = data["wrapperType"] as? String,
            wrapperType == "track",
            let kind = data["kind"] as? String,
            kind == "song",
            let collectionName = data["collectionName"] as? String,
            let trackName = data["trackName"] as? String,
            let collectionCensoredName = data["collectionCensoredName"] as? String,
            let trackCensoredName = data["trackCensoredName"] as? String else {
                return nil
        }
        
        self.collectionName = collectionName
        self.trackName = trackName
        self.collectionCensoredName = collectionCensoredName
        self.trackCensoredName = trackCensoredName
    }
}
