//
//  MusicSearchService.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 20.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

protocol MusicSearchServiceDelegate: AnyObject {
    func onRetrivingArtists(artists: [Artist])
    func onRetrivingAlbums(albums: [Album])
    func onRetrivingSongs(songs: [Song])
}

extension MusicSearchServiceDelegate {
    func onRetrivingArtists(artists: [Artist]) {}
    func onRetrivingAlbums(albums: [Album]) {}
    func onRetrivingSongs(songs: [Song]) {}
}

enum EntityType: Int, CaseIterable, CustomStringConvertible {
    case song, album
    
    var description: String {
        switch self {
        case .song:
            return "song"
        case .album:
            return "album"
        }
    }
}

class MusicSearchService {

    weak var delegate: MusicSearchServiceDelegate?
    
    func findBy(artist: String, page: Int, limit: Int) -> Void {
        let builder = UrlSearchBuilder()
        builder.set(baseUrl: "https://itunes.apple.com/search")
        builder.set(term: artist)
        builder.set(entity: "musicArtist")
        builder.set(page: page)
        builder.set(limit: limit)
        
        let musicApi = AppleMusicApi()
        
        musicApi.findArtisBy(urlString: builder.build(), complition: { serverData in
            var artists: [Artist] = []
            
            for artistData in serverData {
                
                if let artist = Artist(data: artistData) {
                    artists.append(artist)
                }
            }

            DispatchQueue.main.async {
                self.delegate?.onRetrivingArtists(artists: artists)
            }
        })
    }
    
    func lookup(artist: Artist, entity type: EntityType, page: Int, limit: Int) {
        let builder = UrlLookupBuilder()
        builder.set(baseUrl: "https://itunes.apple.com/lookup")
        builder.set(id: String(artist.artistId))
        builder.set(limit: limit)
        builder.set(entity: type.description)
        builder.set(page: page)
        
        let musicApi = AppleMusicApi()
        
        switch type {
        case .album:
            musicApi.lookup(urlString: builder.build()) { serverData in
                var albums: [Album] = []
                
                for albumData in serverData {
                    
                    if let album = Album(data: albumData) {
                        albums.append(album)
                    }
                }

                print("albums length: \(albums.count)")
                DispatchQueue.main.async {
                    print("AppleMusicApi Album before delegate call")
                    self.delegate?.onRetrivingAlbums(albums: albums)
                    print("AppleMusicApi Album after delegate call")
                }
            }
            
            break
            
        case.song:
            musicApi.lookup(urlString: builder.build()) { serverData in
                var songs: [Song] = []

                for songData in serverData {

                    if let song = Song(data: songData) {
                        songs.append(song)
                    }
                }

                print("songs length: \(songs.count)")
                DispatchQueue.main.async {
                    print("AppleMusicApi Song before delegate call")
                    self.delegate?.onRetrivingSongs(songs: songs)
                    print("AppleMusicApi Song after delegate call")
                }
            }
            
            break
        }
    }
    
    deinit {
        print("MusicSearchService deinit")
    }
}
