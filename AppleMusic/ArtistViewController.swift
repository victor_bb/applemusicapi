//
//  ArtistViewController.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 20.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

class Artist {
    let artistId: Int
    let amgArtistId: Int
    let wrapperType: String
    let artistType: String
    let artistName: String
    let artistLinkUrl: String
    let primaryGenreName: String
    let primaryGenreId: Int
    
    init?(data: NSDictionary) {
        guard let artistId = data["artistId"] as? Int,
            let amgArtistId = data["amgArtistId"] as? Int,
            let wrapperType = data["wrapperType"] as? String,
            let artistType = data["artistType"] as? String,
            let artistName = data["artistName"] as? String,
            let artistLinkUrl = data["artistLinkUrl"] as? String,
            let primaryGenreName = data["primaryGenreName"] as? String,
            let primaryGenreId = data["primaryGenreId"] as? Int else {
                return nil
        }
            
        self.artistId = artistId
        self.amgArtistId = amgArtistId
        self.wrapperType = wrapperType
        self.artistType = artistType
        self.artistName = artistName
        self.artistLinkUrl = artistLinkUrl
        self.primaryGenreName = primaryGenreName
        self.primaryGenreId = primaryGenreId
    }
}
