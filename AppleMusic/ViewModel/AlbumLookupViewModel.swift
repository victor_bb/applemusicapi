//
//  LookupViewModel.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 29.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import Foundation

protocol AlbumLookupViewModelDelegate: AnyObject {
    func onAlbumFetchCompleted(with newIndexPathsToReload: [IndexPath]?)
}

class AlbumLookupViewModel {
    
    weak var delegate: AlbumLookupViewModelDelegate?
    
    private var albums: [Album] = []
    private var currentPage = 1
    private var total = 0
    private var isFetchInProgress = false
    private var selectedArtist: Artist?
    
    var totalCount: Int {
        return total
    }
    
    var currentCount: Int {
        return albums.count
    }
    
    func album(at index: Int) -> Album {
        return albums[index]
    }
    
    func lookup(artist: Artist, type: EntityType) -> Void {
        guard !isFetchInProgress else {
            return
        }
        
        if artist != selectedArtist {
            total = 0
            selectedArtist = artist
            albums.removeAll()

            self.currentPage = 1
        } else {
            self.currentPage += 1
        }
        
        isFetchInProgress = true
        
        let musicService = MusicSearchService()
        musicService.delegate = self
        musicService.lookup(artist: artist, entity: type, page: self.currentPage, limit: 20)
    }
    
    private func calculateIndexPathsToReload(from newAlbums: [Album]) -> [IndexPath] {
        let startIndex = albums.count - newAlbums.count
        let endIndex = startIndex + newAlbums.count
        
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}

extension AlbumLookupViewModel: MusicSearchServiceDelegate {
    func onRetrivingAlbums(albums: [Album]) {
        self.total = albums.count
        self.albums.append(contentsOf: albums)
        
        self.isFetchInProgress = false
        
        if self.currentPage > 1 {
            let indexPathsToReload = self.calculateIndexPathsToReload(from: albums)
            self.delegate?.onAlbumFetchCompleted(with: indexPathsToReload)
        } else {
            self.delegate?.onAlbumFetchCompleted(with: .none)
        }
    }
}
