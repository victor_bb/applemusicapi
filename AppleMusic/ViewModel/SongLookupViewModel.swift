//
//  SongLookupViewModel.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 30.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import Foundation

protocol SongLookupViewModelDelegate: AnyObject {
    func onSongsFetchCompleted(with newIndexPathsToReload: [IndexPath]?)
}

class SongLookupViewModel {
    
    weak var delegate: SongLookupViewModelDelegate?
    
    private var songs: [Song] = []
    private var limit = 50
    private var currentPage = 1
    private var total = 0
    private var isFetchInProgress = false
    private var selectedArtist: Artist?

    var totalCount: Int {
      return total
    }
    
    var currentCount: Int {
        return songs.count
    }
    
    func song(at index: Int) -> Song {
        return songs[index]
    }
    
    func lookup(artist: Artist, type: EntityType) -> Void {
        guard !isFetchInProgress else {
            return
        }
        
        if artist != selectedArtist {
            total = 0
            selectedArtist = artist
            songs.removeAll()

            self.currentPage = 1
        } else {
            self.currentPage += 1
        }
        
        isFetchInProgress = true
        
        let musicService = MusicSearchService()
        musicService.delegate = self
        musicService.lookup(artist: artist, entity: type, page: self.currentPage, limit: limit)
    }
    
    private func calculateIndexPathsToReload(from newSongs: [Song]) -> [IndexPath] {
        let startIndex = songs.count - newSongs.count
        let endIndex = startIndex + newSongs.count
        
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}

extension SongLookupViewModel: MusicSearchServiceDelegate {
    func onRetrivingSongs(songs: [Song]) {
        
        self.songs.append(contentsOf: songs)
        self.total = self.songs.count
        
        self.isFetchInProgress = false
        
        if self.currentPage > 1 {
            let indexPathsToReload = self.calculateIndexPathsToReload(from: songs)
            self.delegate?.onSongsFetchCompleted(with: indexPathsToReload)
        } else {
            self.delegate?.onSongsFetchCompleted(with: .none)
        }
    }
}
