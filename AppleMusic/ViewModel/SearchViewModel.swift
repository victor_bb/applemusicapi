//
//  SearchViewModel.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 18.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import Foundation

protocol SearchViewModelDelegate: AnyObject {
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?)
}

class SearchViewModel {
    private var artists: [Artist] = []
    private var currentPage = 1
    private var total = 0
    private var limit = 20
    private var isFetchInProgress = false
    private var searchName = ""
    
    weak var delegate: SearchViewModelDelegate?

    var totalCount: Int {
        return total
    }
    
    var currentCount: Int {
        return artists.count
    }
    
    func artist(at index: Int) -> Artist? {
        if artists.indices.contains(index) {
            return artists[index]
        }
        
        return nil
    }
    
    func findBy(artist: String) -> Void {
        guard !isFetchInProgress else {
            return
        }
        
        if artist != searchName {
            searchName = artist
            total = 0
            artists.removeAll()
            
            self.currentPage = 1
        } else {
            self.currentPage += 1
        }
        
        isFetchInProgress = true
        
        let musicService = MusicSearchService()
        musicService.delegate = self
        musicService.findBy(artist: artist, page: currentPage, limit: limit)
    }
    
    private func calculateIndexPathsToReload(from newArtist: [Artist]) -> [IndexPath] {
      let startIndex = artists.count - newArtist.count
      let endIndex = startIndex + newArtist.count
      return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
    
    deinit {
        print("SearchViewModel deinit")
    }
}

extension SearchViewModel: MusicSearchServiceDelegate {
    func onRetrivingArtists(artists: [Artist]) {
        
        self.artists.append(contentsOf: artists)
        
        if artists.count == self.limit {
            self.total = self.artists.count + 1
        } else if artists.count == 0 {
            self.total = self.artists.count
            // how to remove one cell?
            // or just return
            
            return
        } else if artists.count < self.limit {
            self.total = self.artists.count
        }
        
        self.isFetchInProgress = false
        
        if self.currentPage > 1 {
            let indexPathsToReload = self.calculateIndexPathsToReload(from: artists)
            self.delegate?.onFetchCompleted(with: indexPathsToReload)
        } else {
            self.delegate?.onFetchCompleted(with: .none)
        }
    }
}
