//
//  UrlBuilder.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 20.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

class UrlSearchBuilder {
    
    var baseUrl: String = ""
    
    var urlArgs = [
        "term": "",
        "entity": "",
        "limit": "",
        "offset": ""
    ]
    
    func set(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func set(term: String) {
        urlArgs["term"] = term
    }
    
    func set(entity: String) {
        urlArgs["entity"] = entity
    }
    
    func set(limit: String) {
        urlArgs["limit"] = limit
    }
    
    func set(offset: String) {
        urlArgs["offset"] = offset
    }
    
    func build() -> String {
        
        var argsString = ""
        
        for (key, value) in urlArgs {
            if value.count > 0 {
                argsString += "&\(key)=\(value)"
            }
        }
        
        print("argsString \(argsString)")
        argsString.remove(at: argsString.startIndex)

        return self.baseUrl + "?" + argsString
    }
}
