//
//  AppleMusicApi.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 20.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

class AppleMusicApi {

    func findArtisBy(urlString: String, complition: @escaping ([NSDictionary]) -> Void) {
        
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { responseData, response, error in
            if let data = responseData,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let jsonDictionary = json as? NSDictionary,
                let resultsDictionary = jsonDictionary["results"] as? [NSDictionary] {
                
                complition(resultsDictionary)
            }
        }
        
        task.resume()
    }
    
    func lookup(urlString: String, complition: @escaping ([NSDictionary]) -> Void) {
        
        let url = URL(string: urlString)!
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { responseData, response, error in
            if let data = responseData,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let jsonDictionary = json as? NSDictionary,
                let resultsDictionary = jsonDictionary["results"] as? [NSDictionary] {
                
                complition(resultsDictionary)
            }
        }
        
        task.resume()
    }
    
    deinit {
        print("AppleMusicApi deinit")
    }
}
