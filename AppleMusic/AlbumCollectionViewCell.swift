//
//  AlbumCollectionViewCell.swift
//  AppleMusic
//
//  Created by  Виктор Борисович on 30.06.2020.
//  Copyright © 2020 MyCompany. All rights reserved.
//

import UIKit

let identifier = "albumView"

class AlbumCollectionViewCell: UICollectionViewCell {
    var imagePath: String? {
        didSet {
            let url = URL(string: imagePath!)!
            
            downloadImage(from: url)
        }
    }
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func downloadImage(from url: URL) {
        //print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            //print(response?.suggestedFilename ?? url.lastPathComponent)
            //print("Download Finished")
            DispatchQueue.main.async() { [weak self] in
                self?.imageView.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
